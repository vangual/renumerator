import sys
import os
import argparse
import logging
import re
import operator
import shutil

# Parse command line parameters
parser = argparse.ArgumentParser()
parser.add_argument('-i', '--inputdir', required=True, type=str, help='Specify the input directory')
parser.add_argument('-o', '--outputdir', required=True, type=str, help='Specify the output directory')
parser.add_argument('-p', '--pattern', required=True, type=str,
                    help='Set the file matching pattern. Use {num} as the numerical placeholder in the pattern.')
parser.add_argument('-n', '--dryrun', action='store_true',
                    help="Don't actually modify anything. Just print what would be done.")
parser.add_argument('-l', '--logfile', type=argparse.FileType('w'), help="Optionally specify a logfile to write to.")
parser.add_argument('-s', '--startnum', type=int, default=0,
                    help="Set the starting number used by the new names. Default: 0")
parser.add_argument('--digits', type=int, help="Set the amount of digits used in the filenames. Default: Auto")
args = parser.parse_args()

# Setup logging
log_level = logging.DEBUG  # TODO: Make this an arg
log = logging.getLogger(__name__+'.Logger')
log.setLevel(log_level)
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

if args.logfile:
    file_log_stream = logging.StreamHandler(args.logfile)
    file_log_stream.formatter = formatter
    log.addHandler(file_log_stream)

console_log_stream = logging.StreamHandler()
console_log_stream.formatter = formatter
log.addHandler(console_log_stream)

log.debug('Logging setup.')

# Catch a few common mistakes
if args.inputdir == args.outputdir:
    log.error('Input directory cannot be equal to the output directory!')
    sys.exit(20)

if not os.path.isdir(args.inputdir) or not os.path.isdir(args.outputdir):
    log.error('Error opening directories. Make sure they both exist already!')
    sys.exit(21)

# Prepare the filter
# Assume to get something that contains "bla{num}bla.jpg"
# So first we convert it to a real regex
find_reg_pattern = re.escape(args.pattern).replace('\{num\}', '(\d+)')
log.debug('Looking for pattern "{pat}" in {dir}'.format(pat=find_reg_pattern, dir=args.inputdir))

# Collect the files to work on
found_files = {}  # unsorted dict containing a list of all matched files
try:
    os.chdir(args.inputdir)
    for current_file in os.listdir('.'):
        if re.match(find_reg_pattern, current_file):
            timestamp = os.path.getmtime(current_file)
            found_files[str(current_file)] = timestamp
            log.debug('File match! Name: {fn} Time: {time}'.format(fn=str(current_file), time=timestamp))
        else:
            log.debug('File {fn} did not match.'.format(fn=str(current_file)))

except Exception, e:
    log.debug('Caught an exception when listing files:')
    log.exception(e)

if not found_files:
    log.error('No files matched the given pattern. Exiting.')
    sys.exit(10)

# Determine how many digits we should use in the file names
num_files_found = len(found_files)
auto_digits_to_use = len(str(num_files_found))
log.debug('Determined the need to use {nr} digits to fit all files.'.format(nr=auto_digits_to_use))
digits_to_use = auto_digits_to_use

if args.digits:
    if args.digits < auto_digits_to_use:
        log.error('Requested to use less digits ({req}) than we need ({need}) to cover all files.'.format(
            req=args.digits, need=auto_digits_to_use))
        sys.exit(11)
    else:
        digits_to_use = args.digits

# Sort the dict now
sorted_list = sorted(found_files.iteritems(), key=operator.itemgetter(1))

# Preparing new numbering format
numeric_format = '%0{num}d'.format(num=digits_to_use)
log.debug('Numeric formatter is: {0}'.format(numeric_format))

# Now we start iterating over the found (and sorted) files
count_success = 0
count_fail = 0
for current_idx in range(args.startnum, num_files_found):
    (old_name, old_timestamp) = sorted_list.pop(0)
    new_name = args.pattern.replace('{num}', numeric_format % current_idx)
    log.info('Move {oldname} to {newname}'.format(oldname=old_name, newname=new_name))
    if args.dryrun:
        count_success += 1
        continue  # If we're doing a dryrun, stop here and continue to the next file.
    src = args.inputdir+os.sep+old_name
    dst = args.outputdir+os.sep+new_name
    try:
        log.debug('Moving from {src} to {dst}'.format(src=src, dst=dst))
        shutil.move(src, dst)
        count_success += 1
    except IOError:
        log.warning('Problems moving {src} to {dst}'.format(src=src, dst=dst))
        count_fail += 1

log.info('Done. Processed {success} file(s) successfully, {fail} failed.'.format(success=count_success,
                                                                                 fail=count_fail))
